---
name: Janayugom GNU/Linux
description: Distro created for Janayugom Daily Newspaper
image_path: /images/portfolio/janayugom_gnu_linux.png
url: https://alphafork.com/janayugom
link: https://alphafork.com/janayugom
---
