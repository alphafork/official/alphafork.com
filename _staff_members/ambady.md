---
title: ambady
name: Ambady Anand S
position: Developer
image_path: /images/team/ambady.jpg
mastadon: https://aana.site/@bady
gitlab: https://gitlab.com/bady
wiki:   https://meta.wikimedia.org/wiki/User:Ambadyanands
osm:    https://wiki.openstreetmap.org/wiki/User:Ambadyanands
blurb: Ambady is backend developer
---
