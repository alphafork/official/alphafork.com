---
title: kannan
name: Kannan VM
position: Developer
image_path: /images/team/kannan.jpg
mastadon: https://aana.site/@kannan
gitlab: https://gitlab.com/kannanvm
wiki:   https://meta.wikimedia.org/wiki/User:KannanVM
osm:    https://www.openstreetmap.org/user/Kannan_
blurb: Kannan is Frontend and backend developer
---
