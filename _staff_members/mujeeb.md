---
title: mujeeb
name: Mujeeb Rahman K
position: Co-Founder
image_path: /images/team/mujeeb.jpg
mastadon: https://aana.site/@mujeebcpy
gitlab: https://gitlab.com/mujeebcpy
wiki:   https://meta.wikimedia.org/wiki/User:Mujeebcpy
osm:    https://wiki.openstreetmap.org/wiki/User:Mujeebcpy
blurb: Mujeeb's interest is in teaching and vlogging. 
---
