---
name: Janayugom GNU/Linux
description: Custom-built GNU/Linux Operating System and pre-press solutions for Janayugom Publications
image_path: /images/portfolio/janayugom_gnu_linux.png
url: https://keralapdanam.com
link: janayugom
---
