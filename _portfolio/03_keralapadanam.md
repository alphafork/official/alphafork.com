---
name: Keralapadanam
description: All-Kerala survey web app for Kerala Sasthra Sahithya Parishad
image_path: /images/portfolio/keralapadanam.jpg
url: https://keralapdanam.com
link: keralapadanam
---
