# Alpha Fork Technologies
Official website of Alpha Fork Technologies, hosted at [https://alphafork.com](https://alphafork.com).

Made with [Jekyll](https://jekyllrb.com).

## Prerequisites
* Ruby (`ruby -v`)
* Ruby Gems (`gem -v`)
* GCC (`gcc -v` or `g++ -v`)
* Make (`make -v`)
* Bundler (`bundler -v`)
* Jekyll (jekyll -v`)

## Develop
Install the dependencies with [Bundler](https://bundler.io/):

~~~bash
$ bundle install --path vendor/cache
~~~

Run `jekyll` commands through Bundler to ensure you're using the right versions:

~~~bash
$ bundle exec jekyll serve
~~~

## License
This website is based on the Jekyll theme [Hydra](https://github.com/CloudCannon/hydra-jekyll-template) by [CloudCannon](https://cloudcannon.com/) which is originally released under MIT License.

## Contact
[connect@alphafork.com](mailto:alphafork.com)
