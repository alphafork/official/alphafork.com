---
name: Blog for Kerala Sahithya Akademi.
description: Blog for Kerala Sahithya Akademi using Jekyll SSG.
image_path: /images/portfolio/akademi.jpg
url: http://keralasahityaakademi.org/blog/
link: http://keralasahityaakademi.org/blog/
---
